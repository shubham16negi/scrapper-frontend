import { ScrapperFrontendPage } from './app.po';

describe('scrapper-frontend App', () => {
  let page: ScrapperFrontendPage;

  beforeEach(() => {
    page = new ScrapperFrontendPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
