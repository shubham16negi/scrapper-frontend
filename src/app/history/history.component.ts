import {Component, OnInit} from '@angular/core';
import {ScrapperService} from "../scrapper.service";
import {imageUrl} from '../config';
@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.css'],
  providers: [ScrapperService]

})
export class HistoryComponent implements OnInit {
  imageUrl = imageUrl;
  loading: boolean = null;
  history: { _id: string, keyword: string }[];
  imageList: {
    _id: string,
    images: string[],
    keyword: string
  } = null;

  constructor(private scrapper: ScrapperService) {
  }

  ngOnInit() {
    this.getHistory()
  }

  getHistory() {
    this.loading = true;
    this.scrapper.getHistory().subscribe((data) => {
      this.loading = false;
      let resposne = data.json();
      if (resposne.status === 1000) {
        /* Success */
        this.history = resposne.body;
      } else {
        /* Error */
        alert(resposne.message)
      }
    }, (error) => {
      this.loading = false;
      alert("Error Occured")!
    })
  }

  getImages(id) {
    this.loading = true;
    this.scrapper.getKeywordById(id).subscribe((data) => {
      this.loading = false;
      let resposne = data.json();
      if (resposne.status === 1000) {
        /* Success */
        this.imageList = resposne.body
      } else {
        /* Error */
        alert(resposne.message)
      }
    }, (error) => {
      /* Error */
      this.loading = false;
      alert("Error occured");
    })
  }

}
