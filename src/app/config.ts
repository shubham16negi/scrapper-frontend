export const imageUrl = "http://localhost:3000/scrapedImages/";
export const baseurl = "http://localhost:3000/api/v1";
export const urls = {
  searchKeyword: "/getImage",
  getHistory: "/keywords",
  getKeywordById: "/keyword/",
};
