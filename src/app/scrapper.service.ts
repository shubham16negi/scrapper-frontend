import {Injectable} from '@angular/core';
import {Headers, Http} from '@angular/http';
import {baseurl, urls} from './config';

@Injectable()
export class ScrapperService {

  constructor(private http: Http) {
  }

  headers = new Headers({
    'Content-Type': 'application/json'
  });

  searchKeyWord(keyword) {
    return this.http.post(baseurl + urls.searchKeyword, JSON.stringify({q: keyword}), {headers: this.headers});
  }

  getHistory() {
    return this.http.get(baseurl + urls.getHistory, {headers: this.headers});
  }

  getKeywordById(id) {
    return this.http.get(baseurl + urls.getKeywordById + id, {headers: this.headers});
  }

}
