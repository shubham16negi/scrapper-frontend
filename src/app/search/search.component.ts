import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ScrapperService} from "../scrapper.service";

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css'],
  providers: [ScrapperService]
})
export class SearchComponent implements OnInit {
  searchForm: FormGroup;
  imageList: string[] = [];
  loading: boolean = null;

  constructor(private fb: FormBuilder, private scrapper: ScrapperService) {
    this.searchForm = this.fb.group({
      keyword: ['', Validators.required]
    });
  }

  ngOnInit() {
  }

  submit() {
    this.loading = true;
    this.imageList = [];
    this.scrapper.searchKeyWord(this.searchForm.controls['keyword'].value).subscribe((data) => {
      this.loading = false;
      let resposne = data.json();
      if (resposne.status === 1000) {
        /* Success */
        this.imageList = resposne.body;
      } else {
        /* Error */
        alert(resposne.message)
      }
    }, (error) => {
      this.loading = false;
      console.log(error);
      alert("Something went wrong");
    });
  }

}
