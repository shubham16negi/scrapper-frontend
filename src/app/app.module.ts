import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {SearchComponent} from './search/search.component';
import {HistoryComponent} from './history/history.component';
import {RouterModule, Routes} from '@angular/router';
import {PageNotFoundComponent} from './page-not-found/page-not-found.component';
import {ReactiveFormsModule} from '@angular/forms';
import {HttpModule} from "@angular/http";


const appRoutes: Routes = [
  {path: 'search', component: SearchComponent},
  {path: 'history', component: HistoryComponent},
  {
    path: '',
    redirectTo: '/search',
    pathMatch: 'full'
  },
  {path: '**', component: PageNotFoundComponent}
];


@NgModule({
  declarations: [
    AppComponent,
    SearchComponent,
    HistoryComponent,
    PageNotFoundComponent
  ],
  imports: [
    HttpModule,
    ReactiveFormsModule,
    BrowserModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
